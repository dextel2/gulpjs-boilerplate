/**
 * @author : Yash Karanke
 * @license : MIT
 * This is still un-finished
 */
'use strict';

// if jQuery has not been loaded, load from google cdn
if (!window.jQuery) {
    var s = document.createElement('script');
    s.setAttribute('src', 'https://code.jquery.com/jquery-3.3.1.min.js');
    s.setAttribute('type', 'text/javascript');
    document.getElementsByTagName('head')[0].appendChild(s);
}

/**
 * All Functions goes here
 */
function hello() {
    console.log("Hello");
}

/**
 * Call the function(s)
 */

var app = {
    init: function () {
        hello();
    }
}
/**
 * I'm not sure what this does but let it be here
 * Docs say that it will initalize the app
 */


$(document).ready(function () {
    app.init();
});

module.exports = app;