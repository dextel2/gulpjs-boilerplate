# Boilerplate for Gulp stack

Feel free to use, I'd recommend to use Visual Studio Code
I hope you will understand the tasks in _gulpfile.js_,
if you do not understand tasks feel free to raise an issue

<br>

`gulp` task will _minify_ SASS
<br> _minify_ javascript
<br> _optimize_ images (if required)
<br> and _copy_ fonts and html from `app` dir to `dist` dir

### You only need to publish `dist` dir

LICENSE : MIT

### Node

All the `dependencies` and `devDependencies` are based on node version `v8.11.4` and npm version `v6.4.0`
