const cacheName = 'v1';
const assets = [
    'css/main.css',
    'img/portfolio.jpg',
    'img/projects/project1.jpg',
    'img/projects/project2.jpg',
    'img/projects/project3.jpg',
    'img/projects/project4.jpg',
    'img/projects/project5.jpg',
    'js/app.js',
    'about.html',
    'contact.html',
    'index.html',
    'work.html'
];

self.addEventListener('install', e => {
    e.waitUntil(
        caches
        .open(cacheName)
        .then(cache => {
            // console.log('Service Worker : Caching Files');
            cache.addAll(assets);
        })
        .then(() => self.skipWaiting())
    );
});

self.addEventListener('activate', e => {
    //Remove Unwanted Caches
    e.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames.map(cache => {
                    // console.log("Service Worker : Clearin Old Cache");
                    if (cache !== cacheName) {
                        return caches.delete(cache);
                    }
                })
            )
        })
    );
});

self.addEventListener('fetch', e => {
    e.respondWith(
        fetch(e.request)
        .catch(() => caches.match(e.request))
    )
});