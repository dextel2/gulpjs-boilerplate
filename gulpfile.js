"use strict";
/**
 * @author : Yash Karanke
 * @version : 1.0.0
 * @license : MIT
 */
const gulp = require("gulp"),
	sass = require("gulp-sass"),
	browserSync = require("browser-sync").create(),
	useref = require("gulp-useref"),
	uglify = require("gulp-uglify"),
	del = require("del"),
	concat = require("gulp-concat"),
	imagemin = require("gulp-imagemin"),
	cssnano = require("gulp-cssnano"),
	minifyHtml = require("gulp-minify-html");

gulp.task("default", ["watch"]);

/**
 * @function
 * For Realtime changes
 */

gulp.task(
	"watch", ["sass", "jsbuild", "imagemin", "fonts", "useref"],
	function () {
		gulp.watch("app/scss/**/*.scss", ["sass"]);
		gulp.watch("app/js/**/*.js", ["jsbuild"]);
		gulp.watch("app/images/**/*.+(png|jpg|jpeg|gif|svg)", ["imagemin"]);
		gulp.watch("app/fonts/**/*", ["fonts"]);
		gulp.watch("app/*.html", ["useref"]);
	}
);

/**
 * @function
 * For compiling Sass
 */
gulp.task("sass", function () {
	return gulp
		.src("app/scss/**/*.scss")
		.pipe(sass())
		.pipe(cssnano())
		.pipe(concat("styles.min.css"))
		.pipe(gulp.dest("dist/css"));
});
/**
 * @function
 * task for real time sass loading changes
 * this is not necessary as I have live reload
 */

gulp.task("browserSync", function () {
	browserSync.init({
		server: {
			baseDir: "app"
		}
	});
});

/**
 * @function
 * to minify js and css if there are multiple js and css files
 * in an html file also copies .html from app to dist
 */

gulp.task("useref", function () {
	return gulp
		.src("app/*.html")
		.pipe(useref())
		.pipe(gulp.dest("dist"));
});

/**
 * @function
 * Reduces and optimizes image files mentioned in regex
 */

gulp.task("imagemin", function () {
	return gulp
		.src("app/images/**/*.+(png|jpg|jpeg|gif|svg)")
		.pipe(
			imagemin({
				interlaced: true
			})
		)
		.pipe(gulp.dest("dist/images"));
});

/**
 * @function
 * Copying font from app to dist
 */

gulp.task("fonts", function () {
	return gulp.src("app/fonts/**/*").pipe(gulp.dest("dist/fonts"));
});

/**
 * @function
 * Deleting the entire dist directory
 * 	**BE VERY CAREFUL USING THIS TASK AT YOUR OWN RISK**
 * 	**I HOPE YOU KNOW WHAT YOU'RE DOING USING `del`**
 */

gulp.task("clean:dist", function () {
	return del.sync("dist");
});

/**
 * @function
 * export minified js files
 * It only exports minified js, need to do
 * something like exports.module for better user
 * or maypbe a construtor to init like
 * app.init and then call the function
 */

gulp.task("jsbuild", function () {
	return gulp
		.src("app/js/**/*.js")
		.pipe(uglify())
		.pipe(gulp.dest("dist/js/"));
});

/**
 * @function
 * Minify HTML, how?
 * Lets find out
 * Okay that's great but lets not use it
 */
gulp.task("minifyHtml", function () {
	return gulp
		.src("app/*.html")
		.pipe(minifyHtml())
		.pipe(gulp.dest("dist/"));
});